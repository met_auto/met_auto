import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    public static WebDriver driver;
    @BeforeEach
     public void setup() {
        driver = new ChromeDriver();
    }
    @AfterEach
    public void teardown(){
        driver.quit();
        BaseTest.driver = null;
    }
}