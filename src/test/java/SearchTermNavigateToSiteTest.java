import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SearchTermNavigateToSiteTest extends BaseTest {
    @Test
    public void SearchTermNavigateToSite() {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage(driver, "https://google.com");
        googleSearchPage.dismissModal();
        googleSearchPage.inputTerm("Metasite");
        googleSearchPage.executeSearch();

        GoogleResultsPage googleResultsPage = new GoogleResultsPage(driver);
        Assertions.assertTrue(googleResultsPage.lookupRightPanelAddressContents("Šeimyniškių g. 19"));

        googleResultsPage.clickImagesButton();
        googleResultsPage.findAndClickResultsGroup("metasite business solutions");
        googleResultsPage.findAndClickSearchResultByText("Metasite: Contacts");

        TabSwticher.switchToSecondTab(driver);

        ContactsPage contactsPage = new ContactsPage(driver);
        contactsPage.declinePopup();
        contactsPage.clickSendButton();

        Assertions.assertTrue(contactsPage.validationTriggered(contactsPage.NameInputField));
        Assertions.assertTrue(contactsPage.validationTriggered(contactsPage.EmailInputField));
        Assertions.assertTrue(contactsPage.validationTriggered(contactsPage.CheckboxSection));
    }
}