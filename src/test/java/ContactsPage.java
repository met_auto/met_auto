import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactsPage {
    WebDriver driver;

    @FindBy(how = How.CSS, using = "input[value='Send Now']")
    WebElement SendNowButton;

    @FindBy(how = How.CSS, using = "button#CybotCookiebotDialogBodyButtonDecline")
    WebElement DeclineButton;

    protected By NameInputField = (By.cssSelector("#main_contact_form input[name='your-name']"));
    protected By EmailInputField = (By.cssSelector("#main_contact_form input[name='your-email']"));
    protected By CheckboxSection = (By.cssSelector("#main_contact_form span[class~='wpcf7-checkbox']"));

    public ContactsPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    public void declinePopup() {
        new WebDriverWait(driver,5).
                until(ExpectedConditions.elementToBeClickable(DeclineButton)).click();
    }

    public void clickSendButton(){
        SendNowButton.click();
    }

    public boolean validationTriggered (By element) {
        WebElement elementUnderValidation = driver.findElement(element);
        new WebDriverWait(driver,3).
                until(ExpectedConditions.
                        attributeContains(elementUnderValidation,"class","wpcf7-not-valid"));
        return true;
    }
}
