import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearchPage {
    @FindBy(how = How.XPATH, using = "//button/div[contains(text(),'Reject')] | //button/div[contains(text(),'Atmesti')]")
    WebElement RejectButton;

    @FindBy(how = How.CSS, using =  "input[name=\"q\"]")
    WebElement SearchInput;

    @FindBy(how = How.XPATH, using =  "//div[not (@jsname)]/center/input[contains(@value, 'Google')]")
    WebElement GoogleSearchButton;


    public GoogleSearchPage(WebDriver driver, String URL){
        driver.get(URL);
        PageFactory.initElements(driver,this);
    }

    public void dismissModal(){
        RejectButton.click();
    }

    public void executeSearch(){
        GoogleSearchButton.click();
    }

    public void inputTerm(String searchText){
        SearchInput.sendKeys(searchText);
    }
}
