import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GoogleResultsPage {
    @FindBy(how = How.CSS, using = "div[data-attrid=\"kc:/location/location:address\"] span + span")
    WebElement RightPanelAddress;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Images')] | //a[contains(text(),'Vaizdai')] ")
    WebElement ImagesButton;

    @FindBy(how = How.XPATH, using = "//scrolling-carousel")
    WebElement ResultGroups;

    @FindBy(how = How.CSS, using = " h2~div")
    WebElement InitialSearchResults;

    public GoogleResultsPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    public boolean lookupRightPanelAddressContents(String address) {
        return RightPanelAddress.getText().contains(address);
    }

    public void clickImagesButton() {
        ImagesButton.click();
    }

    public void findAndClickResultsGroup(String resultsGroupName) {
        ResultGroups.findElement(By.xpath("//span[contains(text(),'" + resultsGroupName + "')]")).click();
    }

    public void findAndClickSearchResultByText(String textString){
        InitialSearchResults.findElement(By.xpath("//span[@aria-label='" +textString + "']")).click();
    }
}

