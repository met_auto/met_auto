import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class TabSwticher {

    static public void switchToSecondTab(WebDriver driver){
        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
    }
}
