System requirements to run the test:

1. Chromedriver and Maven must be installed in the host system. The chromedriver for GNU/Linux is bundled in the root folder of this repository, replace the chormedriver for the appropriate operating system if needed.

To run the test:

1. Execute command 'mvn clean test' from within the root folder of the test framework.
2. To run the test and execute an html report run the command 'mvn surefire-report:report', the report will be located in target/site/ folder.

